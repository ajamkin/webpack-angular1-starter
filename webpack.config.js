var path = require('path');
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        app: './app/app.js',
        desktop: './app/desktop/desktop.js',
        secondments: './app/secondments/secondments.js',
        trainings: './app/trainings/trainings.js',
        vacations: './app/vacations/vacations.js',
        vendor: ['angular', 'angular-animate', 'angular-ui-router', 'lodash', 'jquery', 'jquery-ui', 'angular-ui-date', 'angular-ui-bootstrap']
    },
    output: {
        path: path.join(__dirname, 'bundle'),
        filename: '[name].bundle.js'
    },
    module: {
        loaders: [
            { test: /app.*\.js$/, loaders: ['ng-annotate'] },
            { test: /\.html$/, loader: 'raw' },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
            { test: /\.(woff|woff2)$/, loader: "url?prefix=font/&limit=5000" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" },
            { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
            { test: /\.(jpg|png|gif)$/, loader: 'file' }
        ]
    },
    plugins: [
        new ExtractTextPlugin("[name].css"),
        new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.bundle.js"),
        new CleanWebpackPlugin(['bundle']),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
}