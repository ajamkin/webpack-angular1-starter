
/*@ngInject*/
module.exports = function secondmentsConfig($stateProvider) {
    $stateProvider
        .state('secondments', {
            url: '/secondments',
            'abstract': true,
            template: require('./index.html')
        })
        .state('secondments.my', {
            url: '',
            controller: require('./mysecondments/my-secondments.controller'),
            controllerAs: 'vm',
            template: require('./mysecondments/my-secondments.html')
        })
}