module.exports = angular
    .module('myApp.secondments', [])
    .config(require('./secondments.config'));
