require('bootstrap/dist/css/bootstrap.css');
require('jquery-ui/themes/base/jquery-ui.css');
require('./app.css');

angular
    .module('myApp', [
        'ngAnimate',
        'ui.router',
        'ui.date',
        'ui.bootstrap',
        require('oclazyload'),
        require('./shared/shared.module').name,
        require('./desktop/desktop.module').name,
        require('./trainings/trainings.module').name,
        require('./vacations/vacations.module').name,
        require('./secondments/secondments.module').name
    ])
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    })
    .config(function($urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    });