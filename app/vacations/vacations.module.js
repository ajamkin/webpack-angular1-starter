require('./vacations.css');

module.exports = angular
    .module('myApp.vacations', [])
    .config(require('./vacations.config'));
