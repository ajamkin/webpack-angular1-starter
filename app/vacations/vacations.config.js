/*@ngInject*/
module.exports = function vacationsConfig($stateProvider) {
    $stateProvider
        .state('vacations', {
            url: '/vacations',
            'abstract': true,
            template: require('./index.html')
        })
        .state('vacations.my', {
            url: '',
            controller: require('./myvacations/my-vacations.controller'),
            controllerAs: 'vm',
            template: require('./myvacations/my-vacations.html')
        })
}