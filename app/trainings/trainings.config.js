/*@ngInject*/
module.exports = function trainingsConfig($stateProvider) {
    $stateProvider
        .state('trainings', {
            url: '/trainigns',
            'abstract': true,
            template: require('./index.html')
        })
        .state('trainings.my', {
            url: '',
            controller: require('./mytrainings/my-trainings.controller'),
            controllerAs: 'vm',
            template: require('./mytrainings/my-trainings.html')
        })
}