module.exports = angular
    .module('myApp.trainings', [])
    .config(require('./trainings.config'));
