module.exports = angular
    .module('myApp.desktop', [])
    .config(require('./desktop.config'));
