/*@ngInject*/
module.exports = function desktopConfig($stateProvider) {
    $stateProvider
        .state('desktop', {
            url: '',
            'abstract': true,
            template: require('./index.html')
        })
        .state('desktop.my', {
            url: '/',
            controller: require('./mydesktop/my-desktop.controller'),
            controllerAs: 'vm',
            template: require('./mydesktop/my-desktop.html')
        })
}